<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title(); ?> <?php bloginfo('name'); ?>
    </title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script>
        for(var e,l='footer header nav article section aside figure'.split(' ');e=l.pop();document.createElement(e));
    </script>

<?php
wp_head();
 ?>

</head>
<body>
	<header class="web">
		<!--LOGO-->
		<div class="logo">
			<a href="/">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo " />
			</a>
		</div>

		<!--WYSZUKIWARKA-->
		<form action="<?php bloginfo('url') ?>" method="get" accept-charset="UTF-8">
		    <fieldset>
		        <input name="s" type="text" placeholder="Szukaj..." value="" /><input type="submit" value="<?php the_search_query(); ?>" />
		    </fieldset>
		</form>
		<div class="clearfix"></div>

		<!--MENU POZIOME-->
		<nav>
			<!-- <ul>
				<li>
					<a href="/">Start</a>
				</li>
				<li>
					<a href="#">O mnie</a>
				</li>
				<li>
					<a href="#">Sekcje</a>
				</li>
				<li>
					<a href="#">Zapytaj</a>
				</li>
			</ul> -->

      <?php wp_nav_menu(array('theme_location' => 'poziome-menu')); ?>
		</nav>
	</header>
	<div class="clearfix"></div>

	<div class="web">
		<section>
			<!--WPISY-->

      <?php if ( have_posts() ) :
        while ( have_posts() ) : the_post();
        ?>

        <article>
          <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
          <?php the_post_thumbnail('post-icon'); ?>
          <!-- <?php the_content('czytaj dalej'); ?> -->
           <?php the_excerpt(); ?>
          <!-- <?php the_post_thumbnail(); ?> -->
          	<a class="more-link" href="<?php the_permalink(); ?>">Czytaj dalej</a>
        </article>

<?php
      endwhile;

      else:
        echo_e ('Brak wpisow');
    endif; ?>








		</section>

		<!--SIDEBAR-->
		<aside>
			<h2>SEKCJE</h2>
			<nav>

          <?php
          wp_nav_menu(array('theme_location' => "pionowe menuu"));
           ?>


			</nav>

      <?php
       dynamic_sidebar('prawy-sidebar');
       get_sidebar('prawy-sidebar');

       ?>
		</aside>

		<div class="clearfix"></div>
	</div>

	<!--FOOTER-->
	<footer class="web">
		&copy; 2016 WSIiZ
	</footer>
  <?php
  wp_head();
   ?>
</body>
</html>
