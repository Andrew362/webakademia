<?php
  function register_my_menus() {
    register_nav_menus(
      array(
        'poziome-menu' => __('poziome menu')
      )

    );


  add_action('init', 'register_my_menus');

}
add_theme_support('post-thumbnails');

set_post_thumbnail_size(600,300);

add_image_size('post-icon',200,100,true);

register_sidebar(array(
'id' => 'prawy-sidebar',
'name' => 'widdget prawa kolumna',
'before_widget' => '<div class="new-widget">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title' => '</h2>'));


?>
