function pushToLog(content) {
    current = document.getElementById('logger').value;
    separator = current ? '\n' : '';
    newValue = content + '\n' + current;
    document.getElementById('logger').value = newValue;
};

var miniatury = $('.miniatury');


$('#karuzela').on('slid.bs.carousel', function(e) {
    pushToLog('zakonczono zmiane z ' + e.from + ' na ' + e.to);

});
$('#karuzela').on('slide.bs.carousel', function(e) {
    pushToLog('przesuwamy slajd w kierunku ' + e.direction);
    //console.log(e);
    miniatury.eq(e.from).removeClass('myActive');
    miniatury.eq(e.to).addClass('myActive');
});


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});


$('[data-toggle="popover"]').popover();