var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var merge = require('merge-stream');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');


gulp.task('combinejs', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/bootstrap-select/dist/js/bootstrap-select.js',
        'assets/app.js'
    ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public'));
});

gulp.task('combinecss', function () {
    var sassStream, cssStream;
    //compile sass
    sassStream = gulp.src('assets/app.scss')
        .pipe(sass().on('error', sass.logError));
    cssStream = gulp.src([
        'node_modules/bootstrap-select/dist/css/bootstrap-select.css'
    ]);
    return merge(sassStream, cssStream).pipe(cssmin()).pipe(concat('app.css')).pipe(gulp.dest('public'));
});



gulp.task('watch', function () {
    gulp.watch(['assets/**.scss'], ['combinecss']);
    gulp.watch(['assets/**.js'], ['combinejs']);
});


gulp.task('default', ['combinejs', 'combinecss']);