
function isEmpty(str)
  {
        if (str.length == 0)
                return true;
        else
                return false;
  }

  function isWhiteCharacter(str)
  {
        var ws = "\t\n\r ";
        for (i = 0; i < str.length; i++)
        {
                var c = str.charAt(i)
                if ( ws.indexOf(c) == -1)
                        return false;
        }
        return true;
  }

  function checkField(str, msg)
  {
         if (isWhiteCharacter(str) || isEmpty(str))
         {
                 alert(msg);
                 return false;
         }
         return true;
  }


  /*
  * funkcja sprawdzająca inputy
  */
  function checking(form)
  {
         return  checkStringElem(form.elements["f_imie"], 'Błędne imię') &&
                 checkStringElem(form.elements["f_nazwisko"],' Błędne nazwisko') &&
                 emailRegEx(form.elements["f_email"].value) &&
                 checkStringElem(form.elements["f_ulica"],' Błędna ulica/osiedle') &&
                 checkStringElem(form.elements["f_miasto"],' Błędne miasto');

                 /*checkField(form.elements["f_imie"].value, 'Błędne imię') &&
                 checkField(form.elements["f_nazwisko"].value,' Błędne nazwisko') &&
                 checkField(form.elements["f_ulica"].value,' Błędna ulica/osiedle') &&
                 checkField(form.elements["f_miasto"].value,' Błędne miasto') &&
                 emailRegEx(form.elements["f_email"].value);*/
  }


/*
*  Funkcja sprawdzająca poprawnoś email
*/
function emailRegEx(str)
 {
        var mail = /[a-zA-Z_0-9\.]+@[a-zA-Z_0-9\.]+\.[a-zA-Z][a-zA-Z]+/;
        if(mail.test(str))
    		{
                     return true;
    		}

        alert("Podaj poprawny mail");
        return false;
 }

  /*
  *  Funkcja czyszcząca komunikat o błędzie
  */
  function errorClear(objName)
 	{
 			document.getElementById(objName).innerHTML = "";
 	}

  /*
  *  Funkcja wyświetlająca komunikat o błędzie
  */
 	function checkStringElem(obj, msg)
 	{
 			var str = obj.value;
 			var errorFieldName = "e_" + obj.name.substr(2,obj.name.length);

 			if(isWhiteCharacter(str) || isEmpty(str))
 			{
 					document.getElementById(errorFieldName).innerHTML = msg;
 					obj.focus();
 					return false;
 			}
 			errorClear(errorFieldName);
 			return true;
 	}


  /*
  * showElem/hideElem  - obsługa pola nazwisko panieńskie
  */
  function showElem(e)
  {
         document.getElementById(e).style.visibility = 'visible';
  }
  function hideElem(e)
  {
         document.getElementById(e).style.visibility = 'hidden';
  }
