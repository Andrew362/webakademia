$(document).ready(function(){
    var sended_message = [];
    var interval_id = 0;
    $('.load-more-image').click(function(){
        var imgs = $(this).closest('.images').find('.image:not(.loaded)');
        var img = imgs.first().find('img');
        if(imgs.length > 0){
            img.attr('src', img.attr('data-src')).closest('.image').addClass('loaded');
        }
        if(imgs.length <= 1){
            $(this).hide();
        }
    })
    $('.write-to-me').click(function(){
        var name = $(this).data('name');
        var email = $(this).data('email');
        var subject = $(this).data('subject');
        var form = $('#contact form');
        form.find('input[name=Name]').val(name);
        form.find('input[name=Email]').val(email);
        form.find('input[name=Subject]').val(subject);
    });
    $('.send-email').click(function(){
        var form = $(this).closest('form');
        //  Po naciśnięciu przycisku send pobieramy dane z pol formularza i przypisujemy je do pol obiektu.
        //  Objekt ten jest dodawany do tablcy objektow. Poźniej resetujemy nasz slider, a następnie uruchamiamy z nowymi danymi.
        sended_message.push(
            {
                name:       form.find('input[name=Name]').val(),
                email:      form.find('input[name=Email]').val(),
                subject:    form.find('input[name=Subject]').val()
            }
        )
        form.find('input').val('');
        interval_id = resetSlide(interval_id,sended_message);
    })
    interval_id = startSlide(sended_message);
})

function startSlide(slide_data){
    // Przypisanie id z setInterval. Potrzebny do pozniejszego czyszczenia interval.
    var id = setInterval(function () {
        var welcome_text = $('.welcome-text');
        // Jeśli slide_data zawiera jakieś slide'y
        if(slide_data.length){
            // Pobieramy aktualnie wyświetlany index. 
            // Index ten znajduje się w atrybucie data-id. Zwiększamy go i sprawdzamy, czy istnieje element w tablicy z takim indexem.
            var id = parseInt(welcome_text.attr('data-id')) + 1;
            if(typeof slide_data[id] !='undefined'){
                // Jeśli istnieje to wyświetlamy następny text w tablicy
                welcome_text.text('Witaj ' + slide_data[id].name);
                welcome_text.attr('data-id', id);
            }
            // Jeśli nie istnieje to zaczynamy wyświetlanie od indexu 0
            else{
                welcome_text.text('Witaj ' + slide_data[0].name);
                welcome_text.attr('data-id', 0);
            }
        }
    }, 1000, slide_data);
    return id;
}
function resetSlide(id, slide_data){
    clearInterval(id);
    return startSlide(slide_data);
}
