//Stała zawierająca url'e do pobrania danych z API
const url = {
    events: 'http://www.mocky.io/v2/5a9d81a330000042002348b4',
    country: 'http://www.mocky.io/v2/5a9d883830000074002348cc'

}
country = {
    current_id: 0,
    response_api: []
};
$(document).ready(function () {

    $('.js-hide-menu').click(function () {
        $('#mySidebar').animate({ left: -300 }, 300);
        $('.w3-main').animate({ 'margin-left': 0 }, 300);
        $('.js-show-menu').fadeIn();
    })
    $('.js-show-menu').click(function () {
        $('#mySidebar').animate({ left: 0 }, 300);
        $('.w3-main').animate({ 'margin-left': 300 }, 300);
        $(this).fadeOut();
    })
    $('.js-animation-progressbar .progressbar').each(function (item) {
        let percent = $(this).data('percent');
        $(this).animate({ width: percent }, 3000);
    });

    $('.js-load-more-country').click(function(){
       
        loadMore();
    })

    getData({ url: url.events }).then(function (response) {
        loadedEvent(response, 0)
    })
    getData({ url: url.country }).then(function (response) {
        country.response_api = response;
        loadMore();
    })
})

function loadMore() {
    // Sprawdzanie czy zostały pobrane rekordy z API
    if (country.response_api.length) {
        // Przypisujemy wartości początkowe dla pętli
        for (var i = country.current_id, max = country.response_api.length;
            i < max && (i % 5 != 0 || !i);  //  Sprawdzamy, czy obecny i jest mniejsze niż rozmiar tablicy oraz czy reszta z dzielenie i/5 != 0. 
                                            //  Dzięki temu wypisze nam tylko 5 rekordow. Dodatkowo !i. Ponieważ i na początku ma 0, a reszta z dzielenia 0/5 == 0 więc nie wypisze
                                            //  żadnych wrtości na starcie. Więc musimy zanegowac tą wartośc. Najlepiej podstawic pod i wartośc 0 i prześledzic pierwszy krok
            i++) {
            let html = `
                <tr>
                    <td>${country.response_api[i].country}</td>
                    <td>${country.response_api[i].money}</td>
                </tr>
            `
            $('.js-country-section').append(html)
        }
        if(max == i ){
            $('.js-load-more-country').hide();  
        } 
        country.current_id = i+1;
    }
}
/** 
 * @params
 * event: array()
 * current_id: intval
 * 
 * Funkcja przyjmuje dwa argumenty. Tablicę wydarzeń oraz id elementu, ktory będzie dodawany.
 * 
 **/
function loadedEvent(events, current_id) {
    let event = events[current_id];
    // Pobieranie aktualnej godziny
    let date = new Date();
    let time = date.getHours() + ":"
        + date.getMinutes() + ":"
        + date.getSeconds();
    let html = `<tr style="opacity:0">
                    <td>
                        <img src=${event.avatar}
                    </td>
                    <td>${event.event}</td>
                    <td>
                        <i>${time}</i>
                    </td>
                </tr>`;
    // Dodaniawnie przygotowanego html do tablicy oraz jego animacja
    $('.js-async-events').append(html);
    $('.js-async-events tr').last().animate({ opacity: 1 }, 300);

    // Jeśli istnieje następny element w tablicy to wywołaj funkcję (samą siebie -> rekurencja) za 5s
    if (typeof events[current_id + 1] != 'undefined') {
        setTimeout(function () {
            loadedEvent(events, current_id + 1);
        }, 5000, events, current_id)
    }

}

function getData(setting) {

    return $.ajax({
        url: setting.url != null ? setting.url : '',
        dataType: 'jsonp',
        type: 'get'
    })
}