$(document).ready(function() {
    $('select').select2({tags:false,dropdownAutoWidth : true});
});
$(function(){
    var header = $('header');
    $(window).scroll(function() {
            if ($(this).scrollTop() > 0){  
                    header.addClass("sticky");
            }
            else{
                    header.removeClass("sticky");
            }
    });
});

$(document).on('click', '.nav-toggle', function(e){
    e.preventDefault();
    $('#main-menu').animate({
        width: '80%'
    }, 500);
});

$(document).on('click', '.nav-close', function(e){
    var windowWidth = $(window).outerWidth();
    if(windowWidth <= 768){
	e.preventDefault();
	$('#main-menu').animate({
            width: '0%'
        }, 500);
    }
})

$(document).on('click', '#main-menu a', function(e){
    var windowWidth = $(window).outerWidth();

    if(windowWidth <= 768){
        $('#main-menu').animate({
            width: '0%'
        }, 500);
    }
});

$(window).on('resize', function(){
    var navWidth = $('#main-menu');
    var windowWidth = $(window).outerWidth();
    if(windowWidth > 768){
        navWidth.css('width', '100%');
    } else {
        navWidth.css('width', '0%');
    }
});